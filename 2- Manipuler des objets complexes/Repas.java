package sequence2;

public class Repas 
{
	//Déclaration membres attributs
        public String monnaie = "euros";
        private Plat entree;
        private Plat platPrincipal;
        private Plat dessert;
        private double prix;
	
        
        // accesseurs 
        public Plat getEntree() {
			return entree;
		}

	public Plat getPlatPrincipal() {
			return platPrincipal;
		}

	public Plat getDessert() {
			return dessert;
		}

	public double getPrix() {
			return prix;
		}

        
        
	// Constructeurs
	// constructeur par défaut
        public Repas()
        {
		entree = new Plat("créduties à remettre dans l'ordre");
		platPrincipal = new Plat("os de poulet et demie-frite");
		dessert = new Plat("île coulée");
		prix = 12.55;
        }

	
        // Constructeur avec paramètres pour initialiser toutes les propriétés d'instance
		// repas entrée/plat/dessert prix dans la carte
        public Repas(Plat pEntree, Plat pPlatPrincipal, Plat pDessert, double pPrix)
        {
            prix = pPrix;
            platPrincipal = pPlatPrincipal;
            dessert = pDessert;
            entree = pEntree;
        }

        // Constructeur avec paramètres pour initialiser les propriétés d'instance entree, platPrincipal et prix
        // si le type = 1, il s'agit d'un repas entrée/plat, sinon le repas est plat/dessert
        public Repas(Plat pEntreeOuDessert, Plat pPlatPrincipal, double pPrix, int type)
        {
            platPrincipal = pPlatPrincipal;
		if (type==1)
		{
	            entree = pEntreeOuDessert;
		    dessert = null;
		}
		else
		{
		    entree = null;
		    dessert = pEntreeOuDessert;
		}
            prix = pPrix;
        }

        // Constructeur avec paramètres pour initialiser les propriétés d'instance entree, paltPrincipal et dessert
        // repas entree/plat/dessert avec prix imposé.
        public Repas(Plat pEntree, Plat pPlatPrincipal, Plat pDessert)
        {
        	entree = pEntree;
            platPrincipal = pPlatPrincipal;
            dessert = pDessert;
            prix = 12.55;
        }   

}
