package sequence2;

public class Plat {

    // Déclaration membres attributs
    private String descriptif;
    private int nbCalorie;
    public String uniteCalorie; 
 

    // Accesseurs
    public String getDescriptif() {
    	return descriptif;
    }

    public int getNbCalorie() {
    	return nbCalorie;
    }

// Constructeurs
    // Constructeur par défaut - les propriétés sont initialisées avec des valeurs par défaut
    public Plat()
    {
    	this.descriptif = "un plat";
    	this.nbCalorie = 0;
    }

    // Constructeur avec paramètres pour initialiser la propriété d'instance descriptif
	public Plat(String descriptif)
    {
        this.descriptif = descriptif;
        this.nbCalorie = 0;	
    }
	
    // Constructeur avec paramètres pour initialiser les propriétés d'instance descriptif et nbCalorie
    public Plat(String descriptif, int nbCalorie)
    {
        this.descriptif = descriptif;
        this.nbCalorie = nbCalorie;
    }
    
}
