/*
 -------------------------------------------------------
             G�n�ration de la base de donn�es  
 -------------------------------------------------------
*/

/* -------------------------------------------------------
      TABLE : CONSOMMABLES
-------------------------------------------------------- */
create table CONSOMMABLES
  (
     REF_CONSO char(10)  not null  ,
     DESIGN_CONSO varchar(50)  null ,
     constraint PK_CONSOMMABLES primary key (REF_CONSO)
  );

/* ---------------------------------------------------------
      TABLE : FAMILLE
--------------------------------------------------------- */
create table FAMILLE
  (
     NUM_FAM char(5)  not null  ,
     LIB_FAM varchar(32)  null ,
     constraint PK_FAMILLE primary key (NUM_FAM)
  ); 

/* ---------------------------------------------------------
      TABLE : DATE
--------------------------------------------------------- */
create table DATE
  (
     CODE_DATE char(10)  not null,
    constraint PK_DATE primary key (CODE_DATE)
  );

/* ---------------------------------------------------------
      TABLE : TYPE_PANNE
---------------------------------------------------------- */
create table TYPE_PANNE
  (
     CODE_TYP_PANNE char(3)  not null  ,
     NATURE_PANNE varchar(50)  null ,
     constraint PK_TYPE_PANNE primary key (CODE_TYP_PANNE)
  );

/* ---------------------------------------------------------
      TABLE : CENTRE
---------------------------------------------------------- */
create table CENTRE
  (
     CODE_CENT char(3)  not null  ,
     NOM_CENT varchar(32)  null ,
     constraint PK_CENTRE primary key (CODE_CENT)
  ); 

/* ---------------------------------------------------------
      TABLE : LIVRAISONS
---------------------------------------------------------- */
create table LIVRAISONS
  (
     NUM_LIV integer  not null  ,
     NUM_FOUR integer  not null  ,
     CODE_CENT char(3)  not null  ,
     DATE_LIV char(10)  null  ,
     constraint PK_LIVRAISONS primary key (NUM_LIV)
  );

/* --------------------------------------------------------
      TABLE : MATERIELS
--------------------------------------------------------- */

create table MATERIELS
  (
     NUM_MAT integer  not null  ,
     NUM_FOUR integer  not null  ,
     CODE_CENT char(3)  not null  ,
     NUM_FAM char(5)  not null  ,
     NOM_MAT varchar(32)  null  ,
     PRIX_ACHAT_MAT decimal(6,2)  null  ,
     DATE_ACHAT_MAT char(10)  null  ,
     DUREE_GARANTIE_MAT integer  null  ,
     constraint PK_MATERIELS primary key (NUM_MAT)
  );

/* ------------------------------------------------------
      TABLE : FOURNISSEURS
------------------------------------------------------- */
create table FOURNISSEURS
  (
     NUM_FOUR integer  not null  ,
     NOM_FOURN varchar(32)  null  ,
     TEL_FOURN char(15)  null  ,
     RUE_FOURN varchar(32)  null  ,
     CP_FOURN integer null  ,
     VILLE_FOURN varchar(32)  null  ,
     constraint PK_FOURNISSEURS primary key (NUM_FOUR)
  ); 

/* ----------------------------------------------------
      TABLE : PROPOSER
------------------------------------------------------ */
create table PROPOSER
  (
     REF_CONSO char(10)  not null  ,
     NUM_FOUR integer  not null,
     constraint PK_PROPOSER primary key (REF_CONSO, NUM_FOUR)
  ) ;

/* ------------------------------------------------------
      TABLE : UTILISER
------------------------------------------------------ */
create table UTILISER
  (
     NUM_MAT integer  not null  ,
     CODE_CENT char(3)  not null  ,
     CODE_DATE char(10)  not null  ,
     NBR_HEURE integer  null  ,
     constraint PK_UTILISER primary key (NUM_MAT, CODE_CENT, CODE_DATE)
  ); 

/* -----------------------------------------------------
      TABLE : APPARTENIR
------------------------------------------------------ */
create table APPARTENIR
  (
     NUM_LIV integer  not null  ,
     REF_CONSO char(10)  not null  ,
     QTE integer  null  ,
     PRIX_UNITAIRE decimal(5,2)  null ,
     constraint PK_APPARTENIR primary key (NUM_LIV, REF_CONSO)
  ); 

 /*-----------------------------------------------------
      TABLE : REPARER_PANNE
------------------------------------------------------ */
create table REPARER_PANNE
  (
     NUM_MAT integer  not null  ,
     NUM_FOUR integer  not null  ,
     CODE_TYP_PANNE char(3)  not null  ,
     CODE_DATE char(10)  not null  ,
     DATE_REPARATION char(10)  null  ,
     MONTANT_REPARATION decimal(6,2)  null ,
     constraint PK_REPARER_PANNE primary key (NUM_MAT, NUM_FOUR, CODE_TYP_PANNE, CODE_DATE)
  ); 

/* --------------------------------------------------------------------------
      REFERENCES SUR LES TABLES
----------------------------------------------------------------------------- */
alter table LIVRAISONS 
     add constraint FK_LIVRAISONS_FOURNISSEURS foreign key (NUM_FOUR) 
               references FOURNISSEURS (NUM_FOUR);

alter table LIVRAISONS 
     add constraint FK_LIVRAISONS_CENTRE foreign key (CODE_CENT) 
               references CENTRE (CODE_CENT);

alter table MATERIELS 
     add constraint FK_MATERIELS_FOURNISSEURS foreign key (NUM_FOUR) 
               references FOURNISSEURS (NUM_FOUR);

alter table MATERIELS 
     add constraint FK_MATERIELS_CENTRE foreign key (CODE_CENT) 
               references CENTRE (CODE_CENT);

alter table MATERIELS 
     add constraint FK_MATERIELS_FAMILLE foreign key (NUM_FAM) 
               references FAMILLE (NUM_FAM);

alter table PROPOSER 
     add constraint FK_PROPOSER_CONSOMMABLES foreign key (REF_CONSO) 
               references CONSOMMABLES (REF_CONSO);

alter table PROPOSER 
     add constraint FK_PROPOSER_FOURNISSEURS foreign key (NUM_FOUR) 
               references FOURNISSEURS (NUM_FOUR);

alter table UTILISER 
     add constraint FK_UTILISER_MATERIELS foreign key (NUM_MAT) 
               references MATERIELS (NUM_MAT);

alter table UTILISER 
     add constraint FK_UTILISER_CENTRE foreign key (CODE_CENT) 
               references CENTRE (CODE_CENT);

alter table UTILISER 
     add constraint FK_UTILISER_DATE foreign key (CODE_DATE) 
               references DATE (CODE_DATE);

alter table APPARTENIR 
     add constraint FK_APPARTENIR_LIVRAISONS foreign key (NUM_LIV) 
               references LIVRAISONS (NUM_LIV);

alter table APPARTENIR 
     add constraint FK_APPARTENIR_CONSOMMABLES foreign key (REF_CONSO) 
               references CONSOMMABLES (REF_CONSO);

alter table REPARER_PANNE 
     add constraint FK_REPARER_PANNE_MATERIELS foreign key (NUM_MAT) 
               references MATERIELS (NUM_MAT);

alter table REPARER_PANNE 
     add constraint FK_REPARER_PANNE_FOURNISSEURS foreign key (NUM_FOUR) 
               references FOURNISSEURS (NUM_FOUR);

alter table REPARER_PANNE 
     add constraint FK_REPARER_PANNE_TYPE_PANNE foreign key (CODE_TYP_PANNE) 
               references TYPE_PANNE (CODE_TYP_PANNE);

alter table REPARER_PANNE 
     add constraint FK_REPARER_PANNE_DATE foreign key (CODE_DATE) 
               references DATE (CODE_DATE);

/*
 -----------------------------------------------------------------------------
               FIN DE GENERATION
 -----------------------------------------------------------------------------
*/
